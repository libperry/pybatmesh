# pybatmesh

**pybatmesh** is a program to create a wireless mesh network for
communicating with each other. It can be useful during an [internet
shutdown](#internet-shutdown), or to join [online classes](#online-class)
with a group of laptops. It uses [B.A.T.M.A.N. Advanced][batman-adv], an
implementation of the B.A.T.M.A.N. routing protocol to communicate with
peers.

**WARNING**: This program uses an **unencrypted** network. This means you do
not get any more privacy or security than with an open WiFi network.

## Features

- Select best network when multiple nodes have internet access (gateway
  mode)
- Detect insertion and removal of wifi adapters and reconfigure them
- Readable logs
- Supports rfkill

## Requirements

- [systemd-networkd v248 or greater][batman-systemd]
- Linux kernel with [batman-adv v2021.0][batman-systemd] or greater
- [iwd][] for controlling the WiFi adapter
- python (tested on 3.9 and 3.10)
- python setuptools, for building and installing pybatmesh
- [python-systemd][], for logging to systemd journal
- [dasbus][], for communicating with iwd
- two or more machines with a WiFi adapter having ibss support, called
  nodes or peers
- batctl (optional, for debugging)
- python3-pip (for installing dasbus on Debian-based distributions)

## Installing

This program is available in the AUR for Arch users. Users of other
distributions will have to build manually.

### Arch Linux

Install [pybatmesh][aur] (or [pybatmesh-git][aur-devel] for the
development version) from the AUR with your favourite helper:

```sh
yay -S pybatmesh
```

Optionally, [setup systemd-resolved][arch-resolved] for DNS. [Start
pybatmesh][startnx] when you need it.

### Debian or Ubuntu

pybatmesh is not packaged for Debian or Ubuntu, so you will have to build
and install it manually. Currently, only Debian bookworm (testing) and
Ubuntu 21.10 comes with the required version of systemd. Therefore,
pybatmesh requires **Debian testing** or **Ubuntu 21.10 or greater** to
work.

Install the requirements from the repositories:

```sh
# batctl is optional
sudo apt install python3-pip python3-systemd python3-dasbus iwd batctl
```

Now follow the instructions in the
[manual installation section][install-manual]

### Fedora

pybatmesh is not packaged for Fedora, so it should be installed manually.
pybatmesh requires at least systemd v248, which is only available on
**Fedora 34 and above**. python-setuptools support building rpms, which
are the preferred way of installing software in Fedora. Follow the steps
to install pybatmesh on Fedora:

```sh
# Install the build requirements
sudo dnf install python3-setuptools make git

# Clone the repo
git clone https://git.disroot.org/pranav/pybatmesh.git
cd pybatmesh

# Create an installable rpm from the source code
make rpm

# Now install it. This will also install the requirements
sudo dnf install dist/pybatmesh-$(python3 -m setup --version)-1.noarch.rpm
```

### Manually

Verify that the [requirements][] are of required versions.

```sh
# Should be greater than or equal to 2021.0
batctl -v

# 248 or greater
systemctl --version

# Atleast 4.4
uname -r

# >= 3.6
python3 --version

# Check for IBSS (ad-hoc) support in your WiFi firmware or driver
iw phy | grep -q join_ibss && echo "IBSS is supported" || echo "IBSS not supported"
```

Clone the pybatmesh repo and cd into it.

```sh
git clone https://git.disroot.org/pranav/pybatmesh.git
cd pybatmesh
```

Now, build and install pybatmesh:

```sh
make
sudo make install
```

After installing, reload systemd so that it detects the new
service files:

```sh
sudo systemctl daemon-reload
```

To upgrade, clean the build files, update the repo and reinstall:

```sh
make clean
git pull
make
sudo make uninstall
sudo make install
```

This will keep the configuration files.

## How to use

You need more than one machine running pybatmesh for the connection to work.

### Start pybatmesh

Though pybatmesh can run from the command line, it was designed to be
run as a systemd service.
To start pybatmesh, do the command on all the nodes:

```sh
sudo systemctl start pybatmesh.service
```

This will start a mesh network and connect to all nodes. To test if it
works, run `sudo batctl n -w` and check for nodes. If there are any nodes,
your network is up. Press Ctrl+C to stop `batctl`.

### Getting internet access

Connect an ethernet cable from a router to any of the nodes. Now restart
pybatmesh on the node to set `gateway_mode` to `server`. Other nodes will
take a minute or more to renew DHCP. You can optionally do this manually
if you don't want the delay. To do this, type
`sudo networkctl renew bridge0` on all nodes.

### Tethering via WiFi AP

If there are two adapters in a peer, pybatmesh will start a
WiFi ap (also called WiFi hotspot) on one of them.

Connect two WiFi adapters on a node.
Now an AP will be started on one of the adapters.
Type `pybatmesh --print-wifi` to get the WiFi SSID and password.

If you had set up internet access on one of the nodes, internet
can be accessed from the AP.

### Running at boot

To run pybatmesh at boot, enable the service on all the nodes:

```sh
sudo systemctl enable pybatmesh.service
```

Now pybatmesh will start a mesh on every boot.

If you have NetworkManager enabled, which is the default in Ubuntu and
Fedora, it should be disabled:

```sh
sudo systemctl disable NetworkManager.service
```

To stop running at boot, you should disable `pybatmesh.service`:

```sh
sudo systemctl disable pybatmesh.service
```

If you had disabled `NetworkManager` before, enable it:

```sh
sudo systemctl enable NetworkManager.service
```

### Stopping the service

```sh
sudo systemctl stop pybatmesh iwd systemd-networkd systemd-resolved
```

If your distribution uses NetworkManager, starting `pybatmesh.service`
will have stopped it. Start NetworkManager again:

```sh
sudo systemctl start NetworkManager.service
```

### Configuration

pybatmesh comes with a sample configuration file
`/etc/pybatmesh/pybatmesh.conf.example`. To change how the program
behaves, copy it to /etc/pybatmesh/pybatmesh.conf and edit it:

```sh
sudo cp /etc/pybatmesh/pybatmesh.conf{.example,}
# Now edit the file with your favourite editor as root
gedit admin:/etc/pybatmesh/pybatmesh.conf
```

Also, you can change its behaviour every time you run it using
arguments:

```sh
pybatmesh --help
```

## Contributing or reporting bugs

See [HACKING.md](HACKING.md)

## How it works

There are three modes commonly supported by WiFi adapters - `ap` (WiFi
hotspot), `station` (for joining WiFi networks) and `ad-hoc` (or ibss, for
decentralised networks). There are other modes supported by some WiFi
adapters too, like `p2p` (WiFi direct), but this program doesn't use them.

pybatmesh uses two modes - `ad-hoc` and `ap`, for connecting to the
mesh. pybatmesh uses iwd to start an `ad-hoc` network and configures
systemd-networkd to setup a BATMAN Advanced network.
If there are two WiFi adapters connected to the machine,
pybatmesh starts an ad-hoc on one of them and an ap on the other.
You can use the ap for connecting mobile phones and other devices
to the mesh network.

Read the code and the documentation in the code to learn the details.
See the directory [network](network) to see how
systemd-networkd configures the network.

## Use cases

### Online class

pybatmesh can be used to share connections to join online classes. You need
at least one device with internet access if you are not using a program
like [Jami][] (see below).

### Internet shutdown

You can communicate with neighbouring devices running pybatmesh, using
services like [IPFS][], [Jami][], [Briar][] [Secure Scuttlebutt][ssb] or
similar software which can work on an intranet. They should be installed
on your machine _before_ your friendly democratic government announces an
[internet shutdown][], since you cannot download and install them during a
shutdown. When a shutdown occurs, [enable pybatmesh][enable-pybatmesh] and use the
installed software to communicate with anyone within range.

## Uninstalling

If you installed pybatmesh manually, use make uninstall to remove
pybatmesh and its data files. This requires python pip to be installed.

```sh
# Uninstall the program, keeping the config files
sudo make uninstall
# Or, to uninstall and remove config files
sudo make purge
```

## Similar projects

Many projects make setting up B.A.T.M.A.N. Advanced mesh networks with
WiFi routers easier. They are easier to setup and are more
configurable. But pybatmesh is different from them. It simplifies
setting up mesh networks with _laptops or computers_, and was not
made to work with routers.

The following projects does something similar to pybatmesh, but
requires special devices or routers to work. If you live in an area
where the materials are easily available, consider using them instead
of pybatmesh.

- [LibreMesh][]: framework for OpenWrt-based firmwares
- [disaster.radio][]: solar-powered communications network

## License

pybatmesh is [free/libre/swatantra][free-sw] and open source software;
you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation,
eitherversion 3 of the License, or (at your option) any later version.

See [LICENSE](LICENSE) for the complete version of the license.

[batman-adv]: https://www.open-mesh.org/projects/batman-adv/wiki
[internet shutdown]: https://internetshutdowns.in
[ipfs]: https://ipfs.io "InterPlanetary File System"
[jami]: https://jami.net "Peer to peer video calls"
[ssb]: https://scuttlebutt.nz "Secure Scuttlebutt"
[briar]: https://briarproject.org/
[dasbus]: https://github.com/rhinstaller/dasbus "A python D-Bus library"
[aur]: https://aur.archlinux.org/packages/pybatmesh
[aur-devel]: https://aur.archlinux.org/packages/pybatmesh-git
[arch-resolved]: https://wiki.archlinux.org/title/Systemd-resolved#DNS "systemd-resolved on ArchWiki"
[batman-systemd]: https://www.open-mesh.org/news/101 "systemd v248 brings support for batman advanced"
[libremesh]: https://libremesh.org
[disaster.radio]: https://disaster.radio
[startnx]: #start-pybatmesh
[iwd]: https://iwd.wiki.kernel.org "WiFi daemon"
[free-sw]: https://gnu.org/philosophy/free-sw.html "What is free software?"
[enable-pybatmesh]: #running-at-boot
[requirements]: #requirements
[install-manual]: #manually
[python-systemd]: https://github.com/systemd/python-systemd
