# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
__main__.py
-----------

If called as python -m pybatmesh, this file makes pybatmesh run like
it was called from the commandline. Try:
    python -m pybatmesh --help
"""

from pybatmesh.scripts import main

if __name__ == "__main__":
    main()
